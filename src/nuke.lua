local reactor = require("src.reactor")
local component = require("src.component")

return {
    reactor = reactor,
    component = component
}