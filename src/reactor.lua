local function new(width, height)
    local reactor = {}
    local cells = {}

    function reactor.set(x, y, component)
        if x < 1 and x > width or y < 1 and y > height then
            return false
        end

        cells[x + y * width] = component

        return true
    end

    function reactor.get(x, y)
        return cells[x + y * width]
    end

    function reactor.tick()
        -- DO SOMETHING XD
    end

    return reactor
end

local function default()
    return new(9, 6)
end

return {
    new = new,
    default = default
}